﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using bot.Messages;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace bot
{
    public class Bot
    {
        public static void Main(string[] args)
        {
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (var client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                var reader = new StreamReader(stream);
                var writer = new StreamWriter(stream) {AutoFlush = true};
                new Bot(reader, writer, new Join(botName, botKey));
                //new Bot(reader, writer, new CreateRace(new BotId(botName, botKey),"usa"));
            }
        }

        private StreamWriter writer;

        Bot(StreamReader reader, StreamWriter writer, SendMsg join)
        {
            this.writer = writer;
            string line;

            send(join);
            var count = 0;
            double lastAngle = 0;
            int lastPieceIndex = 0;
            double lastDistance = 0;
            double lastSpeed = 0;
            var trackPieces = new List<TrackElement>();
            var lanes = new List<double>();
            var switching = false;
            
            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch (msg.msgType)
                {
                    case "carPositions":
                        var currentElement = ((JArray)msg.data)[0]; //THIS ONLY WORKS FOR ONE CAR, NEED TO FIND OUR CAR AND USE THAT
                        var currentPiece = currentElement["piecePosition"];
                        var currentPieceIndex = currentPiece["pieceIndex"].ToObject<int>();
                        var currentDistance = currentPiece["inPieceDistance"].ToObject<double>();
                        var currentLane = currentPiece["lane"]["endLaneIndex"].ToObject<int>();
                        var currentDistanceForCalc = currentDistance;
                        if (lastPieceIndex != currentPieceIndex)
                        {
                            Console.WriteLine("New Piece");
                            currentDistanceForCalc += trackPieces[lastPieceIndex].Lengths[currentLane];
                            switching = false;//If we have changed pieces, we have finished switching
                        }
                        var speed = currentDistanceForCalc - lastDistance;
                        var acceleration = speed - lastSpeed;
                        var throttleVal = 0.5;
                        var currentTrackPiece = trackPieces[currentPieceIndex];
                        var nextTrackPiece = trackPieces[(currentPieceIndex + 1)%trackPieces.Count];
                        var nextNextTrackPiece = trackPieces[(currentPieceIndex + 2)%trackPieces.Count];

                        var upcomingAngle = nextTrackPiece.Angle;
                        var angleAfterthat = nextNextTrackPiece.Angle;

                        var avgNextRadius = (nextTrackPiece.Radiuses[currentLane] +
                                            nextNextTrackPiece.Radiuses[currentLane]) / 2;
                        //current centrifugal force
                        //Fc = mv^2/r
                        double nextForce = 0;
                        //if (nextTrackPiece.Radiuses[currentLane] > 0)
                        //{
                        //    nextForce = (10*(speed*speed))/nextTrackPiece.Radiuses[currentLane];
                        //}

                        if (avgNextRadius > 0)
                        {
                            nextForce = (10*(speed*speed))/avgNextRadius;
                        }

                        double currentForce = 0;
                        if (currentTrackPiece.Radiuses[currentLane] > 0)
                        {
                            currentForce = (10 * (speed * speed)) / currentTrackPiece.Radiuses[currentLane];
                        }

                        //Console.WriteLine("Distance: {2}, Calc Distance: {3}, Speed: {0}, Acceleration: {1}, Centrifugal force: {4}", speed, acceleration, currentDistance, currentDistanceForCalc, currentForce);

                        //best so far
                        //throttleVal = 0.5 + (0.5 * ((90-upcomingAngle)/90));
                        throttleVal = 1;

                        var max_force = 5;

                        if (nextForce > max_force) //start to slow down
                            throttleVal = 0.56;

                        if (nextForce > max_force && currentForce > max_force) //holy shit stop!
                            throttleVal = 0.01;

                        var angle = Math.Abs(currentElement["angle"].ToObject<double>());
                        if (nextForce > max_force)
                        {
                            //Console.WriteLine("NEXT FORCE TOO HIGH");
                        }

                        if (currentForce > max_force)
                        {
                            //Console.WriteLine("CURRENT FORCE TOO HIGH");
                        }
                            
                            

                        /*if (angle > 0.5)
                        {
                            Console.WriteLine("Angle greater than 2");
                            Console.WriteLine(angle);
                            if (angle > lastAngle)
                            {
                                throttleVal = 0;
                                Console.WriteLine("Angle Increasing");
                            }
                        }*/
                        lastAngle = angle;
                        lastPieceIndex = currentPieceIndex;
                        lastDistance = currentDistance;
                        lastSpeed = speed;
                        if (nextTrackPiece.ContainsSwitch && !switching)
                        {
                            Console.WriteLine("Next Angle: {0}, Next Next Angle: {1}",upcomingAngle,angleAfterthat);
                            if (angleAfterthat < 0)
                            {
                                send(new SwitchMessage("Left"));
                                Console.WriteLine("Switch Left");
                                switching = true;
                                break;
                            }
                            else if (angleAfterthat > 0)
                            {
                                send(new SwitchMessage("Right"));
                                Console.WriteLine("Switch Right");
                                switching = true;
                                break;
                            }
                        }
                        send(new Throttle(throttleVal));
                        count++;
                        if (count % 20 == 0)
                        {
                            //Console.WriteLine(msg.data.ToString());
                            Console.WriteLine(throttleVal);
                        }
                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        Console.WriteLine(msg.data.ToString());
                        send(new Ping());
                        break;
                    case "gameInit":
                        Console.WriteLine("Race init");
                        lanes = ProcessLanes((JArray)((JObject) msg.data)["race"]["track"]["lanes"]);
                        trackPieces = ProcessTrackList((JArray)((JObject)msg.data)["race"]["track"]["pieces"], lanes);
                        //Console.WriteLine(msg.data.ToString());
                        send(new Ping());
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        break;
                    case "crash":
                        Console.WriteLine("Crash");
                        //Console.ReadLine();
                        //Console.WriteLine(msg.data.ToString());
                        send(new Ping());
                        break;
                    case "spawn":
                        Console.WriteLine("Spawn");
                        //Console.WriteLine(msg.data.ToString());
                        send(new Ping());
                        break;
                    default:
                        send(new Ping());
                        break;
                }
            }
        }

        private List<double> ProcessLanes(JArray jsonlanes)
        {
            return jsonlanes.Select(lane => lane["distanceFromCenter"].ToObject<Double>()).ToList();
        }

        private void send(SendMsg msg)
        {
            writer.WriteLine(msg.ToJson());
        }

        private List<TrackElement> ProcessTrackList(JArray track, List<double> lanes)
        {
            var results = new List<TrackElement>();
            foreach (JObject trackElement in track)
            {
                JToken switchToken;
                bool @switch = false;
                if (trackElement.TryGetValue("switch", out switchToken))
                {
                    @switch = switchToken.ToObject<bool>();
                }
                JToken lengthText;
                if (trackElement.TryGetValue("length", out lengthText))
                {
                    var length = lengthText.ToObject<double>();
                    //for straight peice all lanes are same length
                    var laneDistances = lanes.Select(lane => length).ToArray();
                    var radiuses = lanes.Select(lane => (double)0).ToArray();
                    results.Add(new TrackElement('s', 0, laneDistances, radiuses, @switch));
                }
                else
                {
                    var angle = trackElement["angle"].ToObject<double>();
                    var absAngle = Math.Abs(angle);
                    var radius = trackElement["radius"].ToObject<double>();
                    var laneDistances = new List<double>();
                    var laneRadiuses = new List<double>();
                    foreach (var lane in lanes)
                    {
                        var laneRadius = radius + lane;
                        if (absAngle > 0)
                            laneRadius = radius - lane;

                        var length = (absAngle / 360) * 2 * Math.PI * laneRadius;
                        laneDistances.Add(length);
                        laneRadiuses.Add(length);
                    }

                    results.Add(new TrackElement('c', angle, laneDistances.ToArray(), laneRadiuses.ToArray(),@switch));
                }
            }
            return results;
        }
    }
}
