﻿namespace bot
{
    class TrackElement
    {
        public char TrackType;

        public double[] Lengths;

        public double Angle;

        public double[] Radiuses;

        public bool ContainsSwitch;

        public TrackElement(char trackType, double angle, double[] lengths, double[] radiuses, bool containsSwitch)
        {
            TrackType = trackType;
            Lengths = lengths;
            Angle = angle;
            Radiuses = radiuses;
            ContainsSwitch =containsSwitch;
        }
    }
}