﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bot.Messages
{
    public class BotId
    {
        public string name;
        public string key;

        public BotId(string name, string key)
        {
            this.name = name;
            this.key = key;
        }
    }
}
