﻿namespace bot
{
    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}