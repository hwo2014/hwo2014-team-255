﻿using System;

namespace bot
{
    public class SwitchMessage : SendMsg
    {
        public string direction;

        public SwitchMessage(string direction)
        {
            this.direction = direction;
        }

        protected override Object MsgData()
        {
            return this.direction;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}