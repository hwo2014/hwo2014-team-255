﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bot.Messages
{
    public class CreateRace : SendMsg
    {
        public BotId botId;
        public string trackName;

        public CreateRace(BotId botId, string trackName)
        {
            this.botId = botId;
            this.trackName = trackName;
        }

        protected override string MsgType()
        {
            return "createRace";
        }
    }
}
